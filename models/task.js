// create the schema, mode and export the file

const mongoose = require("mongoose");

// create the schema using the mongoose.Schema() function
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending",
    },
});

module.exports = mongoose.model("Task", taskSchema);
