// setup dependencies

const express = require("express");
const mongoose = require("mongoose");

// this allows us to use all the routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute.js");

// create an application using express function
const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// allows all the task routes created in taskRoute.js file to use "/tasks" route
app.use("/tasks", taskRoute)

// database connection
// connecting to MongoDB Atlas
mongoose.connect(
    "mongodb+srv://admin:admin@zuittbatch243.fvxpkh7.mongodb.net/B243-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

app.listen(port, () => {
    console.log(`Server running at port ${port}`);
});