// controller contain the functions and business logic of our ExpressJS application
// meaning all the operations it can do will be placed in this file

const Task = require("../models/task");

// controller functions for getting all tasks

module.exports.getAllTasks = () => {
    return Task.find({}).then((result) => {
        return result;
    });
};

// controller function for creating a task
module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name,
    });

    // save the newly created "newTask" object in the MongoDB database
    return newTask.save().then((task, error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return task;
        }
    });
};

// controller function for deleting a task
// "taskID" is the URL parameter passed from the "taskRoute.js"
// Business Logic
/*
    1. Look for the task with the corresponding id provided in the URL/route
    2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return removedTask;
        }
    });
};

// controller function for updating a task
// Business Logic
/*
    1. Get the task with the id using the Mongoose method "findById"
    2. Replace the task's name returned from the database with the "name" property from the request body
    3. Save the task
*/
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }

        result.name = newContent.name;
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false
            } else {
                return updatedTask;
            }
        })
    });
};
