const express = require("express");

// create a router instance that functions as a middleware and routing system

const router = express.Router();
const taskController = require("../controllers/taskController.js");

// [Section] Routes

// route to get all the tasks
// this route expects to receive a GET request at the URL "/tasks"
router.get("/", (req, res) => {
    taskController
        .getAllTasks()
        .then((resultFromController) => res.send(resultFromController));
});

// route to create a new task
// this route expects to receive a POST request at the URL "/tasks"
router.post("/", (req, res) => {
    // if the information will be coming from the client side, the data can be accessed from the request body
    taskController
        .createTask(req.body)
        .then((resultFromController) => res.send(resultFromController));
});

// route to delete a task
// this route expects to receive a DELETE request at the URL "/tasks/:id"
// the colon (:) is an identifier from the URL, it helps create a dynamic route which allows us to supply information in the URL
router.delete("/:id", (req, res) => {
    taskController
        .deleteTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController));
});

// route to update a task
// this route expects to receive a PU request at the URL "/tasks/:id"
router.put("/:id", (req, res) => {
    taskController
        .updateTask(req.params.id, req.body)
        .then((resultFromController) => res.send(resultFromController));
});

// Use "module.exports" tp export the router object to use in the app.js
module.exports = router;
